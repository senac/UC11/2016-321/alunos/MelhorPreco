package br.com.senac.melhorpreco.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.senac.melhorpreco.R;
import br.com.senac.melhorpreco.model.Oferta;

/**
 * Created by sala304b on 22/09/2017.
 */

public class AdapterOferta extends BaseAdapter {


    private Activity contexto;
    private List<Oferta> lista;


    public AdapterOferta(Activity contexto, List<Oferta> lista) {
        this.contexto = contexto;
        this.lista = lista;
    }


    @Override
    public int getCount() {
        return this.lista.size();
    }

    @Override
    public Object getItem(int i) {
        return  this.lista.get(i) ;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int posicao, View converView, ViewGroup parent ) {

        View view = contexto.getLayoutInflater().inflate(R.layout.item_oferta , parent , false) ;

        Oferta oferta = this.lista.get(posicao) ;

        ImageView  imageView = (ImageView) view.findViewById(R.id.oferta_imagem) ;
        TextView textViewNome = (TextView)view.findViewById(R.id.oferta_nome) ;
        TextView textViewPreco = (TextView)view.findViewById(R.id.oferta_preco) ;
        TextView textViewLocal = (TextView)view.findViewById(R.id.oferta_local) ;
        TextView textViewDataInicio = (TextView)view.findViewById(R.id.oferta_data_inicio) ;
        TextView textViewDataFim = (TextView)view.findViewById(R.id.oferta_data_fim) ;


        textViewNome.setText(oferta.getDesrcicao());
        textViewPreco.setText(String.valueOf(oferta.getPreco()));




        return view;
    }







}
