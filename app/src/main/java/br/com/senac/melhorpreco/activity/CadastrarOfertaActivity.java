package br.com.senac.melhorpreco.activity;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import br.com.senac.melhorpreco.R;

public class CadastrarOfertaActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText campoPesquisa;
    private Button btnpesquisar;

    static final int REQUEST_IMAGE_CAPTURE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_oferta);




    }


    public void capturar(View view) {
        dispatchTakePictureIntent();
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);


        }


    }

    @Override
    public void onClick(View view) {

        Intent it = new Intent(this, MainActivity.class);
        it.putExtra("Produto", btnpesquisar.getText().toString());
        startActivity(it);

        campoPesquisa = (EditText) findViewById(R.id.campo_Pesquisa);
        btnpesquisar = (Button) findViewById(R.id.btnpesquisar);

        btnpesquisar.setOnClickListener(this);


    }




    }



