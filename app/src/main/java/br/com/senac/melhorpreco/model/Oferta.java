package br.com.senac.melhorpreco.model;

import java.util.Date;

/**
 * Created by sala304b on 22/09/2017.
 */

public class Oferta {

    private String descricao  ;
    private double preco ;
    private String local;
    private Date dataInicio ;
    private Date dataFim ;

    public Oferta() {
    }

    public Oferta(String desrcicao) {
        this.descricao = desrcicao;
    }

    public String getDesrcicao() {
        return descricao;
    }

    public void setDesrcicao(String desrcicao) {
        this.descricao = desrcicao;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }
}
