package br.com.senac.melhorpreco.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.senac.melhorpreco.R;
import br.com.senac.melhorpreco.adapter.AdapterOferta;
import br.com.senac.melhorpreco.model.Oferta;

public class MainActivity extends AppCompatActivity {


    private List<Oferta> listaOfertas = new ArrayList<>();
    private List<Oferta> listaOfertasFiltrada = new ArrayList<>();


    private ListView listViewOfertas ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, CadastrarOfertaActivity.class);
                startActivity(intent);


            }
        });




        Oferta oferta1 = new Oferta();
        oferta1.setDesrcicao("Cerveja Heiniken");
        oferta1.setPreco(5);
        oferta1.setDataInicio(new Date());
        oferta1.setDataFim(new Date());
        oferta1.setLocal("Extrabom");

        listaOfertas.add(oferta1);

        listViewOfertas = (ListView) findViewById(R.id.lista_oferta) ;

        AdapterOferta adapter = new AdapterOferta(this , listaOfertas) ;

        listViewOfertas.setAdapter(adapter);










    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);

    }



    public void pesquisar(View view){

        EditText editTextCampoPesquisa = (EditText) findViewById(R.id.campo_Pesquisa)  ;

        String valor = editTextCampoPesquisa.getText().toString() ;

        this.listaOfertasFiltrada.clear();

        for(Oferta o : this.listaOfertas){

            if(o.getDesrcicao().startsWith(valor)){
                listaOfertasFiltrada.add(o);
            }

        }

        AdapterOferta adapterOferta = new AdapterOferta(this , listaOfertasFiltrada);

        listViewOfertas.setAdapter(adapterOferta);




    }




}
